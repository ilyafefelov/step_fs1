package ua.danIt.Application.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ua.danIt.Application.model.User;

public class UserDAO {
  public static HashMap<Integer,User> userHashMap = new HashMap<>();
  public static Set<User> selected = new HashSet<>();

  {
    userHashMap.put(1,new User("Maria",1,"http://podrobnosti.ua/media/pictures/2017/2/13/thumbs/740x415/olga-frejmut-podelilas-trogatelnym-foto-semi_rect_e8fe24661f1215b4b9967fa4ba1430b5.jpg"));
    userHashMap.put(2,new User("Julia",2,"https://www.cambio16.com/wp-content/uploads/2017/08/foto-el-nino-del-meme-triunfal-reaparece-10-anos-despues.jpg"));
    userHashMap.put(3,new User("Mafia",3,"http://www.nationalgeographic.it/images/2017/02/17/121845769-5cd70c95-e40c-4fb6-99fa-955e334fa62b.jpg"));
  }


  public  static ArrayList<User> users = new ArrayList<>();

  public List<User> getAllUsers(){
    return users;
  }

  public Set<User> getSelectedUsers(){
    return selected;
  }

  public void selectUser(Integer userId){
    selected.add(userHashMap.get(userId));
  }

  public User getCurrentUser() {

    for (User user : userHashMap.values()) {
      if (!selected.contains(user)) return user;
    }

    return selected.iterator().next();
  }
}
