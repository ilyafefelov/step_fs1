package ua.danIt.Application.model;

public class User {
  public String name;
  public int id;
  public String url;

  public User(String name, int id, String url) {
    this.name = name;
    this.id = id;
    this.url = url;
  }
  public User(String name, int id) {
    this.name = name;
    this.id = id;

  }


  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }

  public String getUrl() {
    return url;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (id != user.id) return false;
    if (name != null ? ! name.equals(user.name) : user.name != null) return false;
    return url != null ? url.equals(user.url) : user.url == null;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + id;
    result = 31 * result + (url != null ? url.hashCode() : 0);
    return result;
  }
}
