package ua.danIt.Application.model;

public class Message {
  public int id;
  public int fromUserId;
  public int toUserId;
  public String text;

  public Message( int fromUserId, int toUserId, String text) {
    this.fromUserId = fromUserId;
    this.toUserId = toUserId;
    this.text = text;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(int fromUserId) {
    this.fromUserId = fromUserId;
  }

  public int getToUserId() {
    return toUserId;
  }

  public void setToUserId(int toUserId) {
    this.toUserId = toUserId;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
