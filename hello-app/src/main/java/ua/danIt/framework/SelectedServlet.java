package ua.danIt.framework;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.MessageDAO;
import ua.danIt.Application.dao.UserDAO;
import ua.danIt.Application.model.Message;
import ua.danIt.Application.model.User;

public class SelectedServlet extends HttpServlet {
  public  static String head = "<!doctype html>\n" +
      "<html lang=\"en\" xmlns:margin-bottom=\"http://www.w3.org/1999/xhtml\">\n" +
      "<head>\n" +
      "    <meta charset=\"UTF-8\">\n" +
      "    <meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
      "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
      "    <title>Document</title>\n" +
      "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">";


  public static String style = "<style type=\"text/css\">\n" +
      "        .container {\n" +
      "    border: 2px solid #dedede;\n" +
      "    background-color: #f1f1f1;\n" +
      "    border-radius: 5px;\n" +
      "    padding: 10px;\n" +
      "    margin: 10px 0;\n" +
      "}\n" +
      "\n" +
      "/* Darker chat container */\n" +
      ".darker {\n" +
      "    border-color: #ccc;\n" +
      "    background-color: #ddd;\n" +
      "}\n" +
      "\n" +
      "/* Clear floats */\n" +
      ".container::after {\n" +
      "    content: \"\";\n" +
      "    clear: both;\n" +
      "    display: table;\n" +
      "}\n" +
      "\n" +
      "/* Style images */\n" +
      ".container img {\n" +
      "    float: left;\n" +
      "    max-width: 60px;\n" +
      "    width: 100%;\n" +
      "    margin-right: 20px;\n" +
      "    border-radius: 50%;\n" +
      "}\n" +
      "\n" +
      "/* Style the right image */\n" +
      ".container img.right {\n" +
      "    float: right;\n" +
      "    margin-left: 20px;\n" +
      "    margin-right:0;\n" +
      "}\n" +
      "\n" +
      "/* Style time text */\n" +
      ".time-right {\n" +
      "    float: right;\n" +
      "    color: #aaa;\n" +
      "}\n" +
      "\n" +
      "/* Style time text */\n" +
      ".time-left {\n" +
      "    float: left;\n" +
      "    color: #999;\n" +
      "}\n" +
      "    </style>";
  UserDAO userDAO = new UserDAO();
  MessageDAO messageDAO = new MessageDAO();
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    PrintWriter out = resp.getWriter();
    out.write(head);
    out.write(style);
    out.write("</head>");
    out.write("<body>");
    out.write("<h1>"+userDAO.userHashMap.get(Integer.parseInt(req.getPathInfo().substring(1))).name+"</h1>");
    int user = Integer.parseInt(req.getPathInfo().substring(1));
    User current = userDAO.userHashMap.get(user);
    for (Message msg:messageDAO.listMessage){
      if (msg.fromUserId==user&& msg.toUserId==0||
          msg.toUserId==user&& msg.fromUserId==0){
        if (msg.fromUserId ==user){
          out.write("<div class=\"container\">");
          out.write("<img src="+current.url+"alt=\"Avatar\">");
          out.write("<p>"+msg.text+"</p>");
          out.write("</div>");

        }else {
          out.write("<div class=\"container darker\">");
          out.write("<p>"+msg.text+"</p>");
          out.write("</div>");
        }
      }
    }
    out.write("<form action='' method='POST'>");
    out.write("<input class=\"col-sm-offset-1 col-sm-4 col-sm-offset-4 \" name='todoText' type='text'>");
    out.write("<input name='id' type='hidden' value ="+Integer.parseInt(req.getPathInfo().substring(1))+" >");
    out.write("<button class=\"btn col-sm-offset-1 col-sm-2 btn-primary \" type='submit'>Submit</button>");
    out.write("</form>");

    out.write("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String text=req.getParameter("todoText");
    String id = req.getParameter("id");
    int idTo = Integer.parseInt(id);
    MessageDAO.listMessage.add(new Message(0,idTo,text));

    resp.sendRedirect("/message/"+idTo);
  }
}
