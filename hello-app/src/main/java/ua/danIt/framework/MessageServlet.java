package ua.danIt.framework;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.UserDAO;
import ua.danIt.Application.model.User;

/**
 * Created  12.02.2018.
 */
public class MessageServlet extends HttpServlet{

  UserDAO userDAO = new UserDAO();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    PrintWriter out = resp.getWriter();
    out.write(SelectedServlet.head);
    out.write("<body>");

    for (User item : userDAO.getSelectedUsers()) {

      out.write("<form action='' method='POST'>");
      out.write("<button class=\"btn col-sm-offset-4 col-sm-4  btn-primary \" type = 'submit'>");
      out.write(item.name);
      out.write("</button>");
      out.write("<input type=\"hidden\" name='id'value=\""+item.id+"\">");
      out.write("</form>");

    }

    out.write("</body></html>");
  }
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.sendRedirect("/message/"+req.getParameter("id"));
  }
}
