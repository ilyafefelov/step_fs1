package ua.danIt.framework;

import java.io.File;
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import ua.danIt.Application.dao.UserDAO;
import ua.danIt.Application.model.User;

/**
 * Created  12.02.2018.
 */
public class HelloServlet extends HttpServlet {

  UserDAO userDAO = new UserDAO();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    File file = new File("C:\\Users\\kmatl\\Desktop\\step_fs1\\hello-app\\src\\main\\java\\ua\\danIt\\index.html");
    String format = Files.toString(file, Charsets.UTF_8);
    User user = userDAO.getCurrentUser();
    String url = user.url;
    String name = user.name;
    int id = user.id;
    resp.getWriter().write(String.format(format, url,name,id));
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      String input = req.getParameter("nameChoice");
      int userId = Integer.parseInt(input);
      userDAO.selectUser(userId);
      resp.sendRedirect("/");
    }
}
