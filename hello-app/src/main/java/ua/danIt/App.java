package ua.danIt;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.danIt.framework.HelloServlet;
import ua.danIt.framework.MessageServlet;
import ua.danIt.framework.SelectedServlet;


public class App {

  public static void main(String[] args) throws Exception {

    Server server = new Server(8080);
    ServletContextHandler handler = new ServletContextHandler();

    ServletHolder holder = new ServletHolder(new HelloServlet());
    handler.addServlet(holder, "/*");

    MessageServlet messageServlet = new MessageServlet();
    handler.addServlet(new ServletHolder(messageServlet),"/message");

    SelectedServlet selectedServlet = new SelectedServlet();
    handler.addServlet(new ServletHolder(selectedServlet),"/message/*");

    server.setHandler(handler);
    server.start();
    server.join();

  }

}